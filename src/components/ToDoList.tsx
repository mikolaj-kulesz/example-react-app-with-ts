import React from 'react'
import {IItem} from "../App";
import "./TodoList.css"

interface IToDoList {
    items: IItem[]
    deleteHandler: (id: string) => void
}

const ToDoList: React.FC<IToDoList> = ({items, deleteHandler}) => {

    const list = items.map(item => {
        return <li key={item.id}>
            <span>
                {item.id} - {item.text}
            </span>
            <button onClick={() => clickHandler(item.id)}>
                DELETE
            </button>
        </li>
    })

    const clickHandler = (id: string) => {
        deleteHandler(id)
    }

    return (
        <ul>
            {list}
        </ul>
    )
}

export default ToDoList
