import React, {useRef} from 'react'
import "./NewTodo.css"

interface INewToDo {
    submitHandler: (text: string) => void
}

const NewToDo: React.FC<INewToDo> = ({submitHandler}) => {
    const ref = useRef<HTMLInputElement>(null)
    const toDoSubmitHandler = (event: React.FormEvent)=> {
        event.preventDefault()
        const userText = ref.current!.value;
        console.log(userText)
        submitHandler(userText)
        resetInput()
    }

    const resetInput = () => {
        ref.current!.value = ""
    }

    return (
        <form onSubmit={toDoSubmitHandler}>
            <div>
                <label htmlFor="todo-text">ToDo Text:</label>
                <input type="text" id="todo-text" ref={ref}/>
            </div>
            <button type="submit">
                ADD TODO
            </button>
        </form>
    )
}

export default NewToDo
