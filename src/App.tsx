import React, {useState} from 'react';
import ToDoList from "./components/ToDoList";
import NewToDo from "./components/NewToDo";
import {Route} from "react-router-dom"

export interface IItem {
    id: string,
    text: string
}

const App: React.FC = () => {
    const [toDoList, setToDoList] = useState<IItem[]>([
        {
            id: 't1',
            text: 'Finish course T1'
        }
    ])
    const createNewTodo = (t: string) => {
        const newToDo = getNewTodo(t)
        setToDoList([...toDoList, newToDo])
    }
    const getNewTodo = (t: string):IItem => {
        return {
            id: Math.random().toString(),
            text: t
        }
    }
    const deleteItem = (id: string) => {
        const newToDoList = toDoList.filter(item => {
            return item.id !== id
        })
        setToDoList(newToDoList)
    }
    return (
        <div className="App">
            <NewToDo submitHandler={createNewTodo}/>
            <ToDoList items={toDoList} deleteHandler={deleteItem}/>
        </div>
    );
}

export default App;
